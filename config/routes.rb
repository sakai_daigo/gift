Rails.application.routes.draw do
  get 'posts/new'
  get 'npo_users/new'
  get 'npo_users/edit'
  get 'npo_users/show'
  get 'npo_users/index'
  get 'npo_users/about'
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'pays/edit'
  get 'pays/about'
  get 'account_activations/edit'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'serches/index'
  get 'contacts/new'
  get 'contacts/create'
  root 'static_pages#home'
  get '/about', to: "static_pages#about"
  post '/signup', to: 'users#create', as: 'signup'
  get '/signup', to: 'users#new'
  resources :users, only: [:show, :edit, :update, :destroy]
end
