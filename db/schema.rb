# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_28_112646) do

  create_table "contacts", force: :cascade do |t|
    t.text "content"
    t.integer "user_id_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id_id"], name: "index_contacts_on_user_id_id"
  end

  create_table "npo_users", force: :cascade do |t|
    t.integer "user_id_id"
    t.string "first_name"
    t.string "last_name"
    t.integer "post_code"
    t.string "prefecture_code"
    t.string "address_city"
    t.string "address_street"
    t.string "address_building"
    t.integer "phone_number"
    t.string "country"
    t.string "birthday"
    t.string "npo_name"
    t.string "top_image"
    t.string "background_image"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id_id"], name: "index_npo_users_on_user_id_id"
  end

  create_table "pays", force: :cascade do |t|
    t.integer "user_id_id"
    t.text "commant"
    t.integer "amount_money"
    t.integer "npo_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["npo_user_id"], name: "index_pays_on_npo_user_id"
    t.index ["user_id_id"], name: "index_pays_on_user_id_id"
  end

  create_table "posts", force: :cascade do |t|
    t.integer "user_id_id"
    t.string "image"
    t.string "video"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id_id"], name: "index_posts_on_user_id_id"
  end

  create_table "relationships", force: :cascade do |t|
    t.integer "follower_id"
    t.integer "followed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id"
    t.index ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true
    t.index ["follower_id"], name: "index_relationships_on_follower_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nick_name"
    t.string "email"
    t.string "top_image"
    t.string "background_image"
    t.string "activation_digest"
    t.boolean "activated"
    t.string "remember_digest"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
