class CreateNpoUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :npo_users do |t|
      t.references :user_id, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.integer :post_code
      t.string :prefecture_code
      t.string :address_city
      t.string :address_street
      t.string :address_building
      t.integer :phone_number
      t.string :country
      t.string :birthday
      t.string :npo_name
      t.string :top_image
      t.string :background_image
      t.integer :category

      t.timestamps
    end
  end
end
