class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.references :user_id, foreign_key: true
      t.string :image
      t.string :video
      t.text :content

      t.timestamps
    end
  end
end
