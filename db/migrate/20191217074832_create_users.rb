class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nick_name
      t.string :email
      t.string :top_image
      t.string :background_image
      t.string :activation_digest
      t.boolean :activated, defalut: false
      t.string :remember_digest
      t.boolean :admin, defalut: false

      t.timestamps
    end
  end
end
