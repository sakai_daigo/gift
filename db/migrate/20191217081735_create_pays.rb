class CreatePays < ActiveRecord::Migration[5.2]
  def change
    create_table :pays do |t|
      t.references :user_id, foreign_key: true
      t.text :commant
      t.integer :amount_money
      t.references :npo_user, foreign_key: true

      t.timestamps
    end
  end
end
