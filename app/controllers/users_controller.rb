class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def show
    @user = User.find_by(id: params[:id])
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      remember @user
      flash[:success] = "登録が完了しました"
      redirect_to root_path
    else
      render "new"
    end
  end

  private

  def user_params
    params.require(:user).permit(:nick_name, :email, :password,
                                  :password_confirmation)
  end

end
