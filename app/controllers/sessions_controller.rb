class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      remember user
      flash[:success] = "ログインしました"
      redirect_to user
    else
      flash.now[:denger] = "ログインに失敗しました"
      render "new"
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_path
  end

end
