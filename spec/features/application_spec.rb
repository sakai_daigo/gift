require 'rails_helper'

RSpec.feature "Applications", type: :feature do
  describe "application_view" do
    it "indicate correct title" do
      visit root_path
      expect(page).to have_title "Gift | ファンドレイジングプラットフォーム"
      visit about_path
      expect(page).to have_title "Gift | 利用方法について"
      visit npo_users_about_path
      expect(page).to have_title "Gift | 掲載希望団体の方へ"
      visit pays_about_path
      expect(page).to have_title "Gift | 募金方法について"
    end
    it "indicate correct header" do
      visit root_path
      expect(page).to have_link "ログイン"
      expect(page).to have_link "新規登録"
      expect(page).to have_link "Giftとは"
      expect(page).to have_link "お問い合わせ"
      click_on "掲載希望団体の方へ", match: :first
      expect(current_path).to eq npo_users_about_path
      visit root_path
      click_on "募金方法について", match: :first
      expect(current_path).to eq pays_about_path
    end
    it "indicate correct footer" do
      visit root_path
      expect(page).to have_link "すべての団体から探す"
      expect(page).to have_link "サービスについて"
      expect(page).to have_link "問い合わせ"
      expect(page).to have_link "会社概要"
      # 同じパスの同じ名前のリンクが複数ある場合は今回は,二つ目のリンクにクラスを
      # 付けてクリックテストをしたが,他に正しいやり方はある？
      find(".footer-about-npo").click
      expect(current_path).to eq npo_users_about_path
      find(".footer-about-pays").click
      expect(current_path).to eq pays_about_path
    end
  end
end
