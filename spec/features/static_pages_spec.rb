require 'rails_helper'

RSpec.feature "StaticPages", type: :feature do
  describe "static_pages_view" do
    it "indicate correct home" do
      visit root_path
      expect(page).to have_selector "select", text: "カテゴリーから探す"
      expect(page).to have_selector "select", text: "地域から探す"
      expect(page).to have_selector "form"
      expect(page).to have_link "団体一覧へ"
      expect(page).to have_link "動物愛護支援"
      expect(page).to have_link "貧困支援"
      expect(page).to have_link "国際支援"
      expect(page).to have_link "環境保護支援"
      expect(page).to have_link "貧困支援"
      expect(page).to have_link "人権支援"
      expect(page).to have_link "子育て,出産支援"
      expect(page).to have_link "教育支援"
      expect(page).to have_link "災害支援"
    end
    it "indicate correct about" do
      visit about_path
      expect(page).to have_link "支援する"
      expect(page).to have_selector ".about-signup-link", text: "新規登録"
    end
  end
end
