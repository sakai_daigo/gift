require 'rails_helper'

RSpec.describe "StaticPages", type: :request do
  describe "static_pages_controller test" do
    it "home controller" do
      get root_path
      expect(response).to have_http_status(200)
    end
    it "about controller" do
      get about_path
      expect(response).to have_http_status(200)
    end
  end
end
