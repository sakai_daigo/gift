FactoryBot.define do
  factory :contact do
    content { "MyText" }
    user_id { nil }
  end
end
