FactoryBot.define do
  factory :post do
    user_id { nil }
    image { "MyString" }
    video { "MyString" }
    content { "MyText" }
  end
end
