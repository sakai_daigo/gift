FactoryBot.define do
  factory :npo_user do
    user_id { nil }
    first_name { "MyString" }
    last_name { "MyString" }
    post_code { 1 }
    prefecture_code { "MyString" }
    address_city { "MyString" }
    address_street { "MyString" }
    address_building { "MyString" }
    phone_number { 1 }
    country { "MyString" }
    birthday { "MyString" }
    npo_name { "" }
    top_image { "MyString" }
    background_image { "MyString" }
    category { 1 }
  end
end
