FactoryBot.define do
  factory :user do
    nick_name { "MyString" }
    email { "MyString@icloud.com" }
    password { "password" }
    top_image { "account.png" }
    # background_image { "MyString" }
    activation_digest { "MyString" }
    activated { false }
    remember_digest { "MyString" }
    admin { false }
  end
end
