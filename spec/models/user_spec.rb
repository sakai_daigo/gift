require 'rails_helper'

RSpec.describe User, type: :model do

  describe "validates test" do
    let(:user){ User.new(nick_name: "yamada", email: "yamada@icloud.com",
                        password: "foobar", password_confirmation: "foobar") }

    it "should be valide" do
      expect(user.valid?).to be_truthy
    end

    it "name should be present" do
      # 存在性のテストの書き方としてはよくない？
      user.nick_name = ""
      expect(user.valid?).to be_falsey
    end

    it "email should be present" do
      user.email = ""
      expect(user.valid?).to be_falsey
    end

    it "name should not be too long name" do
      user.nick_name = "a" * 51
      expect(user.valid?).to be_falsey
    end

    it "email should not be too long email" do
      user.email = "a" * 245 + "@icloud.com"
      expect(user.valid?).to be_falsey
    end

    it "email validation should reject invalid addresses" do
      invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                          foo@bar_baz.com foo@bar+baz.com foo@bar..com]
       invalid_addresses.each do |invalid_address|
         user.email = invalid_address
         expect(user.valid?).to be_falsey
       end
    end

    it "email should be uniqueness" do
      duplicate_user = user.dup
      duplicate_user.email = user.email.upcase
      user.save
      expect(duplicate_user.valid?).to be_falsey
    end
    # しっかりとデータベースに保存されるときに小文字化されていることを確かめるテスト
    it "email should be saved as downcase" do
       mixed_case_email = "Foo@ExAMPle.CoM"
       user.email = mixed_case_email
       user.save
       expect(user.email).to eq user.reload.email
    end

    it "password should be present (nonblank)" do
      user.password = user.password_confirmation = "" * 6
      expect(user.valid?).to be_falsey
    end

    it "password should have a minimum length" do
      user.password = user.password_confirmation = "a" * 5
      expect(user.valid?).to be_falsey
    end

    it "authenticated? should return false for a user with nil digest" do
      expect(user.authenticated?('')).to be_falsey
    end

  end
end
